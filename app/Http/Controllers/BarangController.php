<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    public function index()
    {
        //mengambil data dari tabel barang_table
        $barang_table = DB::table('barang_table')->get();

        //mengirim data ke view index
        return view('index', ['barang' => $barang_table]);

    }
    
    public function tambah()
    {
        return view('tambah');
    }

    //method untuk insert data ke table barang_table
    public function store(Request $request)
    {
        //insert data ke tabel barang_table
        DB::table('barang_table')->insert([
            'nama_barang' => $request->nama_barang,
            'keterangan' => $request->keterangan
        ]);

        //alihkan ke halaman utama
        return redirect('/');
    }

    public function edit($id)
    {
        //mengambil data barang berdasarkan id yang dipilih
        $barang_table = DB::table('barang_table')->where('id_barang',$id)->get();
        //passing data barang ke view edit.blade.php
        return view('edit', ['barang' => $barang_table]);
    }

    //mengupdate data
    public function update(Request $request)
    {
        //update data barang_table
        DB::table('barang_table')->where('id_barang',$request->id)->update([
            'nama_barang' => $request->namaBrg,
            'keterangan' => $request->ketBrg
        ]);

        //alihkan halaman ke halaman utama
        return redirect('/');
    }

    //menghapus data berdasarkan id
    public function hapus($id)
    {
        //mengahpus data berdasarkan id
        DB::table('barang_table')->where('id_barang',$id)->delete();

        //alihkan halaman ke halaman utama
        return redirect('/');
    }
}
