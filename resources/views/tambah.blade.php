@extends ('template')
@section('title', 'Input Barang')

@section('content')
    <div class="container">
        <div class="card mt-5">
            <div class="card-body">
                <h3 class="text-center">Input Barang</h3>
                <div class="row mt-5">
                    <div class="col md-6">
                        <form action="/store" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="nama_barang">Nama Barang</label>
                                <input type="text" class="form-control" id="nama_barang" name="nama_barang" required="required">
                            </div>
                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <input type="text" class="form-control" id="keterangan" name="keterangan" required="required">
                            </div>
                            <input class="btn btn-success" type="submit" value="Submit">
                            <a href="/" class="btn btn-danger" role="button" aria-pressed="true">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection