@extends ('template')

@section('title', 'Data Barang')

@section ('content')
<div class="container mt-5">
    <table class="shadow p-3 mb-5 table table-striped text-center">
		<thead>
			<tr>
				<th scope="col">No</th>
				<th scope="col">Nama Barang</th>
				<th scope="col">Keterangan</th>
				<th scope="col">Opsi</th>
			</tr>		
		</thead>
		@php
			$n=1;
		@endphp
		@foreach($barang as $p)
		<tr>
			<th scope="row">{{ $n }}</th>
			<td>{{ $p->nama_barang }}</td>
			<td>{{ $p->keterangan }}</td>
			<td>
				<a href="/edit/{{ $p->id_barang }}" class="btn btn-warning" role="button" aria-pressed="true">Edit</a>
				<a href="/hapus/{{ $p->id_barang }}" class="btn btn-danger" role="button" aria-pressed="true">Hapus</a>				
			</td>
			@php
				$n++;
			@endphp
		</tr>
		@endforeach
	</table> 

</div>
@endsection