@extends('template')

@section('title', 'Edit Barang')

@section('content')
<div class="container">
        <div class="card mt-5">
            <div class="card-body">
                <h3 class="text-center">Edit Barang</h3>
                <div class="row mt-5">
                    <div class="col md-6">
                    @foreach($barang as $b)
                        <form action="/update" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="nama_barang">Nama Barang</label>
                                <input type="text" class="form-control" name="namaBrg" required="required" value="{{ $b->nama_barang }}">
                            </div>
                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <input type="text" class="form-control" name="ketBrg" required="required" value="{{ $b->keterangan }}">
                            </div>
                            <input class="btn btn-success" type="submit" value="Submit">
                            <a href="/" class="btn btn-danger" role="button" aria-pressed="true">Cancel</a>
                        </form>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection