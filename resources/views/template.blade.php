<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- BootStarp CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>@yield('title')</title>

    </head>
    <body>
        <!-- NavBar -->
        <nav class="sticky-top shadow p-3 mb-5 navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="/">Ratatouille</a>
            <div class="navbar" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Data Barang</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/tambah">Input Barang</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="main">
            @yield('content')
        </div>
    </body>
</html>
